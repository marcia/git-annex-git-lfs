LOGS

Beginning - till `git annex uninit`

```bash
➜  GitLab git clone git@gitlab.com:marcia/git-annex-git-lfs.git
Cloning into 'git-annex-git-lfs'...
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.
➜  GitLab cd git-annex--git-lfs
cd: no such file or directory: git-annex--git-lfs
➜  GitLab cd git-annex-git-lfs
➜  git-annex-git-lfs git:(master)
```

Repo size: 72 KB

Add `.gitignore` and `logs.md`

```bash
➜  git-annex-git-lfs git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
Untracked files:
  (use "git add <file>..." to include in what will be committed)

  .gitignore
  LOGS.md

nothing added to commit but untracked files present (use "git add" to track)
➜  git-annex-git-lfs git:(master) ✗ git add .
➜  git-annex-git-lfs git:(master) ✗ git commit -m "add .gitignore and logs"
[master 99c4972] add .gitignore and logs
 2 files changed, 649 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 LOGS.md
➜  git-annex-git-lfs git:(master) git push origin master
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (4/4), 4.16 KiB | 0 bytes/s, done.
Total 4 (delta 0), reused 0 (delta 0)
To gitlab.com:marcia/git-annex-git-lfs.git
   1604cc6..99c4972  master -> master
➜  git-annex-git-lfs git:(master)
```

Update logs:

```bash
➜  git-annex-git-lfs git:(master) git add .
➜  git-annex-git-lfs git:(master) git commit -m "update logs"
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
nothing to commit, working tree clean
➜  git-annex-git-lfs git:(master) git push origin master
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 727 bytes | 0 bytes/s, done.
Total 3 (delta 1), reused 0 (delta 0)
To gitlab.com:marcia/git-annex-git-lfs.git
   99c4972..2652b31  master -> master
➜  git-annex-git-lfs git:(master)
```

Repo size: 113 KB

Push Illustrator file

```bash
➜  git-annex-git-lfs git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
Untracked files:
  (use "git add <file>..." to include in what will be committed)

  images/

nothing added to commit but untracked files present (use "git add" to track)
➜  git-annex-git-lfs git:(master) ✗ git add .
➜  git-annex-git-lfs git:(master) ✗ git commit -m "add Illustrator file"
[master 0ce5e7b] add Illustrator file
 1 file changed, 1900 insertions(+)
 create mode 100644 images/background.ai
➜  git-annex-git-lfs git:(master) git push origin master
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (4/4), 357.53 KiB | 0 bytes/s, done.
Total 4 (delta 1), reused 0 (delta 0)
To gitlab.com:marcia/git-annex-git-lfs.git
   2652b31..0ce5e7b  master -> master
➜  git-annex-git-lfs git:(master)
```

- File size (background.ai) = 409 KB
- Repo size: 113 KB (?)

Init git-annex:

```bash
➜  git-annex-git-lfs git:(master) git annex init
init  ok
(recording state in git...)
➜  git-annex-git-lfs git:(master)
```

Track `images/*` files with git-annex:

```bash
➜  git-annex-git-lfs git:(master) git annex add images/*
➜  git-annex-git-lfs git:(master) git annex info
repository mode: indirect
trusted repositories: 0
semitrusted repositories: 4
  00000000-0000-0000-0000-000000000001 -- web
  00000000-0000-0000-0000-000000000002 -- bittorrent
  3d813a49-0878-4baf-9b20-71f4d49a7484 -- ramosmd@MacMarcia.local:~/GitLab/git-annex-git-lfs [here]
  7a7d3d14-70db-4f1c-9045-77f9a56aba08 -- origin
untrusted repositories: 0
transfers in progress: none
available local disk space: 147.7 gigabytes (+1 megabyte reserved)
local annex keys: 0
local annex size: 0 bytes
annexed files in working tree: 0
size of annexed files in working tree: 0 bytes
bloom filter size: 32 mebibytes (0% full)
backend usage:
➜  git-annex-git-lfs git:(master)
```

Push after initiating git-annex and tracking files:

```bash
➜  git-annex-git-lfs git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

  modified:   LOGS.md

no changes added to commit (use "git add" and/or "git commit -a")
➜  git-annex-git-lfs git:(master) ✗ git add .
➜  git-annex-git-lfs git:(master) ✗ git commit -m "update logs, push after git annex add"
[master 4dec730] update logs, push after git annex add
 1 file changed, 32 insertions(+)
➜  git-annex-git-lfs git:(master) git annex sync --content
commit
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
nothing to commit, working tree clean
ok
pull origin
remote: Counting objects: 2, done.
remote: Total 2 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (2/2), done.
From gitlab.com:marcia/git-annex-git-lfs
 * [new branch]      git-annex  -> origin/git-annex
ok
(merging origin/git-annex into git-annex...)
(recording state in git...)
push origin
Counting objects: 8, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (7/7), done.
Writing objects: 100% (8/8), 1.33 KiB | 0 bytes/s, done.
Total 8 (delta 3), reused 0 (delta 0)
remote:
remote: To create a merge request for synced/git-annex, visit:
remote:   https://gitlab.com/marcia/git-annex-git-lfs/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fgit-annex
remote:
remote: To create a merge request for synced/master, visit:
remote:   https://gitlab.com/marcia/git-annex-git-lfs/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fmaster
remote:
To gitlab.com:marcia/git-annex-git-lfs.git
 * [new branch]      git-annex -> synced/git-annex
 * [new branch]      master -> synced/master
ok
➜  git-annex-git-lfs git:(master)
```

Repo size: 113 KB

Push changes to this file ang Illustrator file:

```bash
➜  git-annex-git-lfs git:(master) ✗ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

  modified:   LOGS.md
  modified:   images/background.ai

no changes added to commit (use "git add" and/or "git commit -a")
➜  git-annex-git-lfs git:(master) ✗ git add .
➜  git-annex-git-lfs git:(master) ✗ git commit -m "update logs, update background.ai"
[master 27acea9] update logs, update background.ai
 2 files changed, 1754 insertions(+), 1900 deletions(-)
 rewrite images/background.ai (66%)
➜  git-annex-git-lfs git:(master) ✗ git annex sync --content
commit
[master ab38f3b] git-annex in ramosmd@MacMarcia.local:~/GitLab/git-annex-git-lfs
 1 file changed, 1 insertion(+), 1 deletion(-)
ok
pull origin
ok
push origin
Counting objects: 8, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (7/7), done.
Writing objects: 100% (8/8), 317.03 KiB | 0 bytes/s, done.
Total 8 (delta 4), reused 0 (delta 0)
remote:
remote: To create a merge request for synced/master, visit:
remote:   https://gitlab.com/marcia/git-annex-git-lfs/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fmaster
remote:
To gitlab.com:marcia/git-annex-git-lfs.git
   4dec730..ab38f3b  master -> synced/master
ok
➜  git-annex-git-lfs git:(master)
```

Repo size: 911 KB (looks like now Git identified the ai file (?))

It looks like git-annex didn't annex my file at all:

```bash
➜  git-annex-git-lfs git:(master) ✗ git annex info
repository mode: indirect
trusted repositories: 0
semitrusted repositories: 4
  00000000-0000-0000-0000-000000000001 -- web
  00000000-0000-0000-0000-000000000002 -- bittorrent
  3d813a49-0878-4baf-9b20-71f4d49a7484 -- ramosmd@MacMarcia.local:~/GitLab/git-annex-git-lfs [here]
  7a7d3d14-70db-4f1c-9045-77f9a56aba08 -- origin
untrusted repositories: 0
transfers in progress: none
available local disk space: 147.7 gigabytes (+1 megabyte reserved)
local annex keys: 0
local annex size: 0 bytes
annexed files in working tree: 0
size of annexed files in working tree: 0 bytes
bloom filter size: 32 mebibytes (0% full)
backend usage:
➜  git-annex-git-lfs git:(master) ✗
```

Trying again:

➜  git-annex-git-lfs git:(master) ✗ git annex add images/background.ai
➜  git-annex-git-lfs git:(master) ✗ git annex info
repository mode: indirect
trusted repositories: 0
semitrusted repositories: 4
  00000000-0000-0000-0000-000000000001 -- web
  00000000-0000-0000-0000-000000000002 -- bittorrent
  3d813a49-0878-4baf-9b20-71f4d49a7484 -- ramosmd@MacMarcia.local:~/GitLab/git-annex-git-lfs [here]
  7a7d3d14-70db-4f1c-9045-77f9a56aba08 -- origin
untrusted repositories: 0
transfers in progress: none
available local disk space: 147.7 gigabytes (+1 megabyte reserved)
local annex keys: 0
local annex size: 0 bytes
annexed files in working tree: 0
size of annexed files in working tree: 0 bytes
bloom filter size: 32 mebibytes (0% full)
backend usage:
➜  git-annex-git-lfs git:(master) ✗ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

  modified:   LOGS.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

  images/background-v1.png

no changes added to commit (use "git add" and/or "git commit -a")
➜  git-annex-git-lfs git:(master) ✗ git annex info
repository mode: indirect
trusted repositories: 0
semitrusted repositories: 4
  00000000-0000-0000-0000-000000000001 -- web
  00000000-0000-0000-0000-000000000002 -- bittorrent
  3d813a49-0878-4baf-9b20-71f4d49a7484 -- ramosmd@MacMarcia.local:~/GitLab/git-annex-git-lfs [here]
  7a7d3d14-70db-4f1c-9045-77f9a56aba08 -- origin
untrusted repositories: 0
transfers in progress: none
available local disk space: 147.68 gigabytes (+1 megabyte reserved)
local annex keys: 0
local annex size: 0 bytes
annexed files in working tree: 0
size of annexed files in working tree: 0 bytes
bloom filter size: 32 mebibytes (0% full)
backend usage:
➜  git-annex-git-lfs git:(master) ✗ git annex add images/*
add images/background-v1.png ok
(recording state in git...)
➜  git-annex-git-lfs git:(master) ✗ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

  new file:   images/background-v1.png

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

  modified:   LOGS.md
  modified:   images/background.ai

➜  git-annex-git-lfs git:(master) ✗ git annex add .
add LOGS.md ok
add images/background.ai ok
(recording state in git...)
➜  git-annex-git-lfs git:(master) ✗ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

  typechange: LOGS.md
  new file:   images/background-v1.png
  typechange: images/background.ai

➜  git-annex-git-lfs git:(master) ✗ git commit -m "modify ai file, add png img"
[master 456a597] modify ai file, add png img
 3 files changed, 3 insertions(+), 1890 deletions(-)
 rewrite LOGS.md (100%)
 mode change 100644 => 120000
 create mode 120000 images/background-v1.png
 rewrite images/background.ai (100%)
 mode change 100644 => 120000
➜  git-annex-git-lfs git:(master) git annex sync --content
commit
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
nothing to commit, working tree clean
ok
pull origin
ok
copy LOGS.md (checking origin...) (to origin...)
SHA256E-s8529--bf74eab0a69cc1d41925fb6a510f48e327c32ec00e83650d424f4c9ee931fb59.md
        8529 100%    0.00kB/s    0:00:00 (xfer#1, to-check=0/1)

sent 8692 bytes  received 42 bytes  2495.43 bytes/sec
total size is 8529  speedup is 0.98
ok
copy images/background-v1.png (checking origin...) (to origin...)
SHA256E-s90662--b8ae71f912322310f54c066ff229f6cd47165947f4955752a7db1c357ab40c3b.png
       90662 100%   55.21MB/s    0:00:00 (xfer#1, to-check=0/1)

sent 90835 bytes  received 42 bytes  7902.35 bytes/sec
total size is 90662  speedup is 1.00
ok
copy images/background.ai (checking origin...) (to origin...)
SHA256E-s365773--34295de90df5091c3e8852e6a40dcb445965bd88667228508488ace6dde4f687.ai
      365773 100%   52.93MB/s    0:00:00 (xfer#1, to-check=0/1)

sent 365982 bytes  received 42 bytes  15575.49 bytes/sec
total size is 365773  speedup is 1.00
ok
pull origin
ok
(recording state in git...)
push origin
Counting objects: 30, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (24/24), done.
Writing objects: 100% (30/30), 2.57 KiB | 0 bytes/s, done.
Total 30 (delta 6), reused 0 (delta 0)
remote:
remote: To create a merge request for synced/git-annex, visit:
remote:   https://gitlab.com/marcia/git-annex-git-lfs/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fgit-annex
remote:
remote: To create a merge request for synced/master, visit:
remote:   https://gitlab.com/marcia/git-annex-git-lfs/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fmaster
remote:
To gitlab.com:marcia/git-annex-git-lfs.git
   75110a7..dcb92bd  git-annex -> synced/git-annex
   ab38f3b..456a597  master -> synced/master
ok
➜  git-annex-git-lfs git:(master)
```

Now it's working, both png and ai are symlinks.

```bash
➜  git-annex-git-lfs git:(master) git annex info
repository mode: indirect
trusted repositories: 0
semitrusted repositories: 4
  00000000-0000-0000-0000-000000000001 -- web
  00000000-0000-0000-0000-000000000002 -- bittorrent
  3d813a49-0878-4baf-9b20-71f4d49a7484 -- ramosmd@MacMarcia.local:~/GitLab/git-annex-git-lfs [here]
  7a7d3d14-70db-4f1c-9045-77f9a56aba08 -- origin
untrusted repositories: 0
transfers in progress: none
available local disk space: 146.67 gigabytes (+1 megabyte reserved)
local annex keys: 3
local annex size: 464.96 kilobytes
annexed files in working tree: 3
size of annexed files in working tree: 464.96 kilobytes
bloom filter size: 32 mebibytes (0% full)
backend usage:
  SHA256E: 3
➜  git-annex-git-lfs git:(master)
```


Unannex LOGS.md, annexed by mistake:

```bash
➜  git-annex-git-lfs git:(master) git annex unannex LOGS.md
unannex LOGS.md ok
➜  git-annex-git-lfs git:(master) ✗ git info
git: 'info' is not a git command. See 'git --help'.

Did you mean one of these?
  init
  mailinfo
➜  git-annex-git-lfs git:(master) ✗ git annex info
repository mode: indirect
trusted repositories: 0
semitrusted repositories: 4
  00000000-0000-0000-0000-000000000001 -- web
  00000000-0000-0000-0000-000000000002 -- bittorrent
  3d813a49-0878-4baf-9b20-71f4d49a7484 -- ramosmd@MacMarcia.local:~/GitLab/git-annex-git-lfs [here]
  7a7d3d14-70db-4f1c-9045-77f9a56aba08 -- origin
untrusted repositories: 0
transfers in progress: none
available local disk space: 146.67 gigabytes (+1 megabyte reserved)
local annex keys: 3
local annex size: 464.96 kilobytes
annexed files in working tree: 2
size of annexed files in working tree: 456.44 kilobytes
bloom filter size: 32 mebibytes (0% full)
backend usage:
  SHA256E: 2
➜  git-annex-git-lfs git:(master) ✗
```

Repo size: 1.3 MB

Stopping Annex:

```bash
➜  git-annex-git-lfs git:(master) ✗ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
Untracked files:
  (use "git add <file>..." to include in what will be committed)

  LOGS.md

nothing added to commit but untracked files present (use "git add" to track)
➜  git-annex-git-lfs git:(master) ✗ git add .
➜  git-annex-git-lfs git:(master) ✗ git commit -m "unannex logs.md"
[master 05cc21e] unannex logs.md
 1 file changed, 462 insertions(+)
 create mode 100644 LOGS.md
➜  git-annex-git-lfs git:(master) git annex sync --content
commit
On branch master
Your branch is ahead of 'origin/master' by 2 commits.
  (use "git push" to publish your local commits)
nothing to commit, working tree clean
ok
pull origin
remote: Counting objects: 11, done.
remote: Compressing objects: 100% (8/8), done.
remote: Total 11 (delta 6), reused 0 (delta 0)
Unpacking objects: 100% (11/11), done.
From gitlab.com:marcia/git-annex-git-lfs
   dcb92bd..ab2396d  git-annex  -> origin/git-annex
ok
(merging origin/git-annex into git-annex...)
push origin
Counting objects: 5, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (5/5), done.
Writing objects: 100% (5/5), 3.13 KiB | 0 bytes/s, done.
Total 5 (delta 1), reused 0 (delta 0)
remote:
remote: To create a merge request for synced/git-annex, visit:
remote:   https://gitlab.com/marcia/git-annex-git-lfs/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fgit-annex
remote:
remote: To create a merge request for synced/master, visit:
remote:   https://gitlab.com/marcia/git-annex-git-lfs/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fmaster
remote:
To gitlab.com:marcia/git-annex-git-lfs.git
   dcb92bd..ab2396d  git-annex -> synced/git-annex
   456a597..05cc21e  master -> synced/master
ok
➜  git-annex-git-lfs git:(master) git annex direct
commit
[master 7f7b6c1] commit before switching to direct mode
 1 file changed, 11 insertions(+)
ok
direct images/background-v1.png ok
direct images/background.ai ok
direct  ok
➜  git-annex-git-lfs git:(annex/direct/master) git annex uninit
unannex images/background-v1.png ok
unannex images/background.ai ok
git-annex: Not fully uninitialized
Some annexed data is still left in .git/annex/objects/
This may include deleted files, or old versions of modified files.

If you don't care about preserving the data, just delete the
directory.

Or, you can move it to another location, in case it turns out
something in there is important.

Or, you can run `git annex unused` followed by `git annex dropunused`
to remove data that is not used by any tag or branch, which might
take care of all the data.

Then run `git annex uninit` again to finish.

➜  git-annex-git-lfs git:(annex/direct/master) git annex indirect
commit  (recording state in git...)

ok
(recording state in git...)
[annex/direct/master 5132308] commit before switching to indirect mode
 2 files changed, 2 deletions(-)
 delete mode 120000 images/background-v1.png
 delete mode 120000 images/background.ai
ok
indirect  ok
ok
➜  git-annex-git-lfs git:(master) ✗ git status
On branch master
Your branch is ahead of 'origin/master' by 2 commits.
  (use "git push" to publish your local commits)
Untracked files:
  (use "git add <file>..." to include in what will be committed)

  images/

nothing added to commit but untracked files present (use "git add" to track)
➜  git-annex-git-lfs git:(master) ✗ git add .
➜  git-annex-git-lfs git:(master) ✗ git coomit -m "git annex uninit"
git: 'coomit' is not a git command. See 'git --help'.

Did you mean this?
  commit
➜  git-annex-git-lfs git:(master) ✗ git commit -m "git annex uninit"
[master 2b9d328] git annex uninit
 2 files changed, 1945 insertions(+)
 create mode 100644 images/background-v1.png
 create mode 100644 images/background.ai
➜  git-annex-git-lfs git:(master) git push origin master
Counting objects: 10, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (10/10), done.
Writing objects: 100% (10/10), 403.04 KiB | 0 bytes/s, done.
Total 10 (delta 4), reused 0 (delta 0)
To gitlab.com:marcia/git-annex-git-lfs.git
   05cc21e..2b9d328  master -> master
➜  git-annex-git-lfs git:(master)
```

- Repo size: 1.3 MB
- Images are back in `master`
- Delete branches (GitLab UI), except `master`
- Push after deleting remote branches

Repo Size: 1.8 MB





